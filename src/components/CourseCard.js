import React from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}) {
const {name, description, price} = courseProp;
    return(
        <Row>
            <Col>
                <Card className="cardHighlight p-3 m-3">
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
};